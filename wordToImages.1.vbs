On Error Resume Next
Dim dataFolderIn
Dim dataFolderOut
dataFolderIn = wscript.arguments.item(0)
dataFolderOut = wscript.arguments.item(1)

Set objFSO = CreateObject("Scripting.FileSystemObject")
If (Not(objFSO.FolderExists(dataFolderIn))) Then
WScript.Echo "Folder does not exists : " & dataFolderIn
WScript.Quit
End If
If (Not(objFSO.FolderExists(dataFolderOut))) Then
objFSO.CreateFolder(dataFolderOut)
End If
convertWordToImage dataFolderIn,dataFolderOut
Set objFSO = nothing

Sub convertWordToImage(dataFolderIn,dataFolderOut)
	On Error Resume Next
	Set objShell = CreateObject("WScript.Shell")
	Set objExec = objShell.Exec("taskkill /IM xps2img.exe")
	Set objExec = objShell.Exec("taskkill /IM WINWORD.EXE")
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objDataFolderIn = objFSO.GetFolder(dataFolderIn)
	Set objDataFolderOut = objFSO.GetFolder(dataFolderOut)
	WScript.Echo "function"
	Set colFiles = objDataFolderIn.Files
	WScript.Echo "Total Number Of Files To Convert Are : " & colFiles.Count
	Dim fileExt
	For Each objFile in colFiles
		fileExt = Split(objFile.Name,".")
		If (fileExt(1) = "doc" Or fileExt(1) = "docx") Then
			WScript.Echo "Processing : " & objFile.Name
			convertWordToXPS_XPSToImage objFile,objDataFolderOut
		End If
	Next
	Set objShell = nothing
	Set objExec = nothing
	Set objFSO = nothing
End Sub


Sub convertWordToXPS_XPSToImage(objFile,objDataFolderOut)
		On Error Resume Next
		Set objFSO = CreateObject("Scripting.FileSystemObject")
		Set objShell = CreateObject("WScript.Shell")
		Set objWord = CreateObject("Word.Application")
		Set objDoc = objWord.Documents.Open(objFile.Path)
		Dim pageCnt
		pageCnt = objDoc.ComputeStatistics(2)
		WScript.Echo "Page Count Of " & objFile.Name & " Is : " & pageCnt & " Pages"
		Set objExec = objShell.Exec("cmd /c mkdir " & objDataFolderOut & "\Xps")
		objDoc.SaveAs objDataFolderOut & "\Xps\" & objFile.Name & ".xps" , 18
		WScript.Echo "Converted  Word To XPS : " &  objFile.Name 
		objDoc.Close
		objWord.Quit
		
		Dim xpsToImageCoverterEXE 
		xpsToImageCoverterEXE = "C:\Program Files\XPS to Images Converter\xps2img.exe"
		Set objShell = CreateObject("WScript.Shell")
		Dim xpsFilePath
		xpsFilePath = objDataFolderOut & "\Xps\" & objFile.Name & ".xps"
		name = Split(objFile.Name,".")
		objFSO.CreateFolder(objDataFolderOut & "\" & name(0))
		WScript.Echo "Converting XPS To PNG  : " &  objFile.Name & ".xps"
		Set objExec = objShell.Exec(xpsToImageCoverterEXE & " " & """" & xpsFilePath & """" & " " & """" & objDataFolderOut & "\" & name(0) & """" & " -r 759 -d 2350")
		waitForXpsToImageConversion()
		'Rename file is not working when xps2imagege process got stuck and
		'when it got ed by timeout.So commenting it.
		'renameFiles objDataFolderOut & "\" & name(0)
		WScript.Echo "Converted XPS To PNG  : " &  objFile.Name & ".xps"
		Set objExec = objShell.Exec("taskkill /IM WINWORD.exe")	
		Set objShell = nothing
		Set objWord = nothing
		Set objDoc = Nothing
		Set objFSO = nothing
End Sub

Function convertSingleToDoubleDigit(number)
	On Error Resume Next
	Select Case number
		Case 1
			convertSingleToDoubleDigit = "01"
		Case 2
			convertSingleToDoubleDigit = "02"
		Case 3
			convertSingleToDoubleDigit = "03"
		Case 4
			convertSingleToDoubleDigit = "04"
		Case 5
			convertSingleToDoubleDigit = "05"
		Case 6
			convertSingleToDoubleDigit = "06"
		Case 7
			convertSingleToDoubleDigit = "07"
		Case 8
			convertSingleToDoubleDigit = "08"
		Case 9
			convertSingleToDoubleDigit = "09"
	End Select
End Function

Sub renameFiles(dataFolderOut)
        On Error Resume Next
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(dataFolderOut)
	Set colFiles = objFolder.Files
	WScript.Echo "Renaming png images: " & objFolder.Name 
	For Each objFile In colFiles
		oldName = objFile.Name
		Dim oldNameArr
		oldNameArr = Split(oldName,".png")
		Dim intPage 
		intPage = CInt(oldNameArr(0))
		if  10 > intPage Then
			convPageNo = convertSingleToDoubleDigit(intPage)
			newName = objFolder.Name & "_Image_"& convPageNo & ".png"
			objFile.Name = newName
		else
			newName = objFolder.Name & "_Image_"& oldName 
			objFile.Name = newName
		end if
	Next
	WScript.Echo "Renaming Complete."
	Set objFSO = nothing
End Sub


Sub waitForXpsToImageConversion()
	On Error Resume Next
	const timeout=120000    'in millisecond(2 Minutes)
	const timepoll=500    'in millisecond

	set svc=getobject("winmgmts:root\cimv2")
	sQuery="select * from win32_process where name='xps2img.exe'"
	set cproc=svc.execquery(sQuery)
	iniproc=cproc.count    'it can be more than 1
	if iniproc=0 then
		wscript.echo "The process wfica32.exe is inexistent. Operation aborted."
		set cproc=nothing : set svc=nothing
		wscript.quit(1)
	end if
	set cproc=nothing
	for i=0 to timeout\timepoll
		set cproc=svc.execquery(sQuery)
		if cproc.count=0 then    'this monitor all instances died out
			wscript.echo "All processes of xps2img.exe are terminated."
                        exit for
		else
			if i=timeout\timepoll then
				wscript.echo "Some process xps2img.exe is still running within the timeout period."
			else
				wscript.sleep timepoll
			end if
		end if
	next
	set cproc=nothing	
	set svc=nothing
End Sub





