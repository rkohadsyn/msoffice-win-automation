
Sub openExcelFiles(dataFolderIn)
	Set objShell = CreateObject("WScript.Shell")
	Set objExec = objShell.Exec("taskkill /IM EXCEL.EXE")
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objDataFolderIn = objFSO.GetFolder(dataFolderIn)
	Set colFiles = objDataFolderIn.Files
	Dim fileExt
	For Each objFile in colFiles
		fileExt = Split(objFile.Name,".")
		If (fileExt(1) = "xls" Or fileExt(1) = "xlsx") Then
			openExcelFile objFile
		End If
	Next
	Set objShell = nothing
	Set objExec = nothing
	Set objFSO = nothing
End Sub


Sub openExcelFile(objFile)
		Set objShell = CreateObject("WScript.Shell")
		Set objExcel = CreateObject("Excel.Application")
		objExcel.Visible = TRUE
		Set objWork = objExcel.Workbooks.Open(objFile.Path)
		WScript.Sleep 2000
		objWork.Close
		objExcel.Quit
		Set objExec = objShell.Exec("taskkill /IM EXCEL.exe")	
		Set objShell = nothing
		Set objExcel = nothing
		Set objWork = nothing
		Set objExec = nothing
End Sub


On Error Resume Next
Dim dataFolderIn
dataFolderIn = wscript.arguments.item(0)
openExcelFiles dataFolderIn



