###############################################################################################
# docx_convt_doc.pl
#
# Description:
#
#	Script for converting the 2007 docx file into 2010 docx file
#	Launch Word 2010 and open existing 2007 file, Save As Word Document (2010) file 
#
# Usage: >docx_convt_doc.pl
###############################################################################################
 # xlAddIn  Microsoft Office Excel Add-In.  
 # xlCSV  Comma separated value.  
 # xlCSVMac  Comma separated value.  
 # xlCSVMSDOS  Comma separated value.  
 # xlCSVWindows  Comma separated value.  
 # xlDBF2  Dbase 2 format.  
 # xlDBF3  Dbase 3 format.  
 # xlDBF4  Dbase 4 format.  
 # xlDIF  Data Interchange format.  
 # xlExcel2  Excel version 2.0.  
 # xlExcel2FarEast  Excel version 2.0 far east.  
 # xlExcel3  Excel version 3.0.  
 # xlExcel4  Excel version 4.0.  
 # xlExcel5  Excel version 5.0.  
 # xlExcel7  Excel 95.  
 # xlExcel9795  Excel version 95 and 97.  
 # xlExcel4Workbook  Excel version 4.0. Workbook format.  
 # xlIntlAddIn  Microsoft Office Excel Add-In international format.  
 # xlIntlMacro  Deprecated format.  
 # xlWorkbookNormal  Excel workbook format.  ********* Does not work, saves in 2003 format only
 # xlSYLK  Symbolic link format.  
 # xlTemplate  Excel template format.  
 # xlCurrentPlatformText  Specifies a type of text format  
 # xlTextMac  Specifies a type of text format.  
 # xlTextMSDOS  Specifies a type of text format.  
 # xlTextPrinter  Specifies a type of text format.  
 # xlTextWindows  Specifies a type of text format.  
 # xlWJ2WD1  Deprecated format.  
 # xlWK1  Lotus 1-2-3 format.  
 # xlWK1ALL  Lotus 1-2-3 format.  
 # xlWK1FMT  Lotus 1-2-3 format.  
 # xlWK3  Lotus 1-2-3 format.  
 # xlWK4  Lotus 1-2-3 format.  
 # xlWK3FM3  Lotus 1-2-3 format.  
 # xlWKS  Lotus 1-2-3 format.  
 # xlWorks2FarEast  Microsoft Works 2.0 format  
 # xlWQ1  Quattro Pro format.  
 # xlWJ3  Deprecated format.  
 # xlWJ3FJ3  Deprecated format.  
 # xlUnicodeText  Specifies a type of text format.  
 # xlHtml  Web page format.  
 # xlWebArchive  MHT format.  
 # xlXMLSpreadsheet  Excel Spreadsheet format.  
 # xlExcel12  Excel12  
 # xlOpenXMLWorkbook  Open XML Workbook  **********
 # xlOpenXMLWorkbookMacroEnabled  Open XML Workbook Macro Enabled  
 # xlOpenXMLTemplateMacroEnabled  Open XML Template Macro Enabled  
 # xlTemplate8  Template 8  
 # xlOpenXMLTemplate  Open XML Template  
 # xlAddIn8  Microsoft Excel 97-2003 Add-In  
 # xlOpenXMLAddIn  Open XML Add-In  
 # xlExcel8  Excel8  
 # xlOpenDocumentSpreadsheet  OpenDocument Spreadsheet  **************
 # xlWorkbookDefault  Workbook default  



#!/usr/bin/perl
#!C:/Perl/bin

use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Excel';
	
# get the path of input directory
chomp ($in = $ARGV[0]);

# get the pathe of output directory
chomp ($out = $ARGV[1]);
print "$in\n";
print "$out\n";

# Create a filehandler for input and output directory
opendir (SOURCE,$in) or die "Can't access directory\n";
@allfiles = readdir(SOURCE);

opendir (DEST,$out) or die "Can't access directory\n";

closedir(SOURCE);
closedir(DEST);

foreach $f (@allfiles)
{
    if (!(($f eq ".") || ($f eq "..")))
    {
        my $ObjectClass = "Excel.Application";
		
		# Assigne the in and out file into a variables.
		my $infile  = $in."\\".$f;
		print "$infile\n";
		
		# Change the extension of the doc file into docx format
        my $secfile = $f;
		if ($secfile =~ /xls$/)
		{
			$secfile =~s/xls$/xlsx/i;
		}
    
		my $outfile = $out."\\".$secfile;
		print "$outfile\n";
  
        eval {$ex = Win32::OLE->GetActiveObject($ObjectClass)};
        die "Excel not installed" if $@;
        unless (defined $ex) {
            $ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;})
                    or die "Oops, cannot start Excel";
        }
		# Make Excel visible
		$ex->{'Visible'} = 1;
		sleep(2);
		
		# NO UNABLE TO SAVE ERROR
		
		if (defined $ex)
		{
			# Open the doc file
			my $File = $ex->Workbooks->Open($infile) || die("Unable to open document ", Win32::OLE->LastError());
			# Save the above file as doc format
			$File->SaveAs($outfile,xlOpenXMLWorkbook) || warn("Unable to Save As document: $!", Win32::OLE->LastError());
			#$ex->ActiveDocument->SaveAs({FileName => $outfile, FileFormat => xlOpenXMLWorkbook}) || warn("Unable to Save As document ", Win32::OLE->LastError());
			# File Close
			$File->Close(); 
		}
		else
		{
			die "Unable to get Excel! $!/n";
		}
		# Quit Excel
		$ex->Quit();
    }
}