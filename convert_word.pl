###############################################################################################
# docx_convt_doc.pl
#
# Description:
#
#	Script for converting the 2007 docx file into 2010 docx file
#	Launch Word 2010 and open existing 2007 file, Save As Word Document (2010) file 
#
# Usage: >docx_convt_doc.pl
###############################################################################################
# type
  # WdSaveFormat = TOleEnum;
# const
  # wdFormatDocument = 0;	*** Saves in original format 2003
  # wdFormatDocument97 = 0;
  # wdFormatTemplate = 1;
  # wdFormatTemplate97 = 1;
  # wdFormatText = 2;
  # wdFormatTextLineBreaks = 3;
  # wdFormatDOSText = 4;
  # wdFormatDOSTextLineBreaks = 5;
  # wdFormatRTF = 6;
  # wdFormatUnicodeText = 7;
  # wdFormatEncodedText = 7;
  # wdFormatHTML = 8;
  # wdFormatWebArchive = 9;
  # wdFormatFilteredHTML = 10;
  # wdFormatXML = 11;
  # wdFormatXMLDocument = 12;	********
  # wdFormatXMLDocumentMacroEnabled = 13;
  # wdFormatXMLTemplate = 14;
  # wdFormatXMLTemplateMacroEnabled = 15;
  # wdFormatDocumentDefault = 16;	********
  # wdFormatPDF = 17;
  # wdFormatXPS = 18;
  # wdFormatOpenDocumentText = 23;
 
#!/usr/bin/perl
#!C:/Perl/bin

use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Word';
	
# get the path of input directory
chomp ($in = $ARGV[0]);

# get the pathe of output directory
chomp ($out = $ARGV[1]);
print "$in\n";
print "$out\n";

# Create a filehandler for input and output directory
opendir (SOURCE,$in) or die "Can't access directory\n";
@allfiles = readdir(SOURCE);

opendir (DEST,$out) or die "Can't access directory\n";

closedir(SOURCE);
closedir(DEST);

foreach $f (@allfiles)
{
    if (!(($f eq ".") || ($f eq "..")))
    {
        my $ObjectClass = "Word.Application.12";
		
		# Assigne the in and out file into a variables.
		my $infile  = $in."\\".$f;
		print "$infile\n";
		
		# Change the extension of the doc file into docx format
        my $secfile = $f;
		if ($secfile =~ /doc$/)
		{
			$secfile =~s/doc$/docx/i;
		}
    
		my $outfile = $out."\\".$secfile;
		print "$outfile\n";
		
		# eval {$ex = Win32::OLE->GetActiveObject($ObjectClass)};
		# die "Word not installed" if $@;
		# unless (defined $ex) {
			# $ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;})
					# or die "Oops, cannot start Word";
		# }

		$ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;}) or die "Oops, cannot start Word";
					
		# Make Word visible
		$ex->{'Visible'} = 1;
		sleep(2);

		if (defined $ex)
		{	
			# Open the doc file
			print "ex = $ex\n";
			if (my $File = $ex->Documents->Open($infile))
			{
				# Save the above file as doc format
				$File->SaveAs($outfile,wdFormatDocumentDefault) || warn("Unable to Save As document: $err", Win32::OLE->LastError());
				#$ex->ActiveDocument->SaveAs({FileName => $outfile, FileFormat => wdFormatDocumentDefault}) || warn("Unable to Save As document ", Win32::OLE->LastError());
				#File Close
				$File->Close(wdSaveChanges); 
			}
			else
			{
				warn("Unable to open document ", Win32::OLE->LastError());			
			}
		}
		else
		{
			die "Unable to get Word! $!/n";
		}
		
		# Quit Word
		$ex->Quit(wdSaveChanges);
		$ex = undef;
    }
}