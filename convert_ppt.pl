###############################################################################################
# docx_convt_doc.pl
#
# Description:
#
#	Script for converting the 2007 docx file into 2010 docx file
#	Launch Word 2010 and open existing 2007 file, Save As Word Document (2010) file 
#
# Usage: >docx_convt_doc.pl
###############################################################################################

#!/usr/bin/perl
#!C:/Perl/bin

use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft PowerPoint';
	
# get the path of input directory
chomp ($in = $ARGV[0]);

# get the pathe of output directory
chomp ($out = $ARGV[1]);
print "$in\n";
print "$out\n";

# Create a filehandler for input and output directory
opendir (SOURCE,$in) or die "Can't access directory\n";
@allfiles = readdir(SOURCE);

opendir (DEST,$out) or die "Can't access directory\n";

closedir(SOURCE);
closedir(DEST);

foreach $f (@allfiles)
{
    if (!(($f eq ".") || ($f eq "..")))
    {
        my $ObjectClass = "PowerPoint.Application";
		
		# Assigne the in and out file into a variables.
		my $infile  = $in."\\".$f;
		print "$infile\n";
		
		# Change the extension of the doc file into docx format
        my $secfile = $f;
		if ($secfile =~ /ppt$/)
		{
			$secfile =~s/ppt$/pptx/i;
		}
    
		my $outfile = $out."\\".$secfile;
		print "$outfile\n";
  
        eval {$ex = Win32::OLE->GetActiveObject($ObjectClass)};
        die "Powerpoint not installed" if $@;
        unless (defined $ex) {
            $ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;})
                    or die "Oops, cannot start Powerpoint";
        }
		
		#Unable to open document OLE exception from "Microsoft Office PowerPoint 2007":
		#Presentations.Open : Invalid request.  The PowerPoint Frame window does not exist.
		#Win32::OLE(0.1709) error 0x80048240
		#in METHOD/PROPERTYGET "Open" at convert_ppt2010.pl line 65.
		
		#Unable to save document: 0 
		
		# Make Powerpoint visible
		$ex->{'Visible'} = 1;
		sleep(2);
		if (defined $ex)
		{
			# Open the doc file
			my $File = $ex->Presentations->Open($infile) || die("Unable to open document ", Win32::OLE->LastError());
			# Save the above file as doc format
			# http://answers.microsoft.com/en-us/office/forum/office_2007-customize/presentationsaveas-method-using/15ba26aa-ac45-e011-9bac-78e7d160ad4e?msgId=9fce6c9d-c845-e011-9575-d8d385dcbb12
			my $err = $File->SaveAs($outfile,ppSaveAsOpenXMLPresentation) || warn("Unable to Save As document: $err", Win32::OLE->LastError());
			#$File->SaveAs($outfile,ppSaveAsPresentation) || warn("Unable to Save As document: $!", Win32::OLE->LastError());
			#$ex->ActiveDocument->SaveAs({FileName => $outfile, FileFormat => ppSaveAsPresentation}) || warn("Unable to Save As document ", Win32::OLE->LastError());
			# File Close
			$File->Close(); 
		}
		else
		{
			die "Unable to get Powerpoint! $!/n";
		}
		# Quit Powerpoint
		$ex->Quit(); 
    }
}