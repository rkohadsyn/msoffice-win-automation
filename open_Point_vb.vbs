
Sub openPointFiles(dataFolderIn)
	Set objShell = CreateObject("WScript.Shell")
	Set objExec = objShell.Exec("taskkill /IM POWERPNT.EXE")
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objDataFolderIn = objFSO.GetFolder(dataFolderIn)
	Set colFiles = objDataFolderIn.Files
	Dim fileExt
	For Each objFile in colFiles
		fileExt = Split(objFile.Name,".")
		If (fileExt(1) = "ppt" Or fileExt(1) = "pptx") Then
			openPointFile objFile
		End If
	Next
	Set objShell = nothing
	Set objExec = nothing
	Set objFSO = nothing
End Sub


Sub openPointFile(objFile)
		Set objShell = CreateObject("WScript.Shell")
		Set objPoint = CreateObject("PowerPoint.Application")
		objPoint.Visible = TRUE
		objPoint.Presentations.Open(objFile.Path)
		Set objPres = objPoint.ActivePresentation
		WScript.Sleep 2000
		objPres.Close
		objPoint.Quit
		Set objExec = objShell.Exec("taskkill /IM POWERPNT.exe")	
		Set objShell = nothing
		Set objPoint = nothing
		Set objPres = nothing
		Set objExec = nothing
End Sub


On Error Resume Next
Dim dataFolderIn
dataFolderIn = wscript.arguments.item(0)
openPointFiles dataFolderIn



