use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Excel';

chomp ($in = $ARGV[0]);
print "in = $in\n";

opendir (SOURCE,$in) or die $!;
@allfiles = readdir(SOURCE);
closedir(SOURCE);

foreach $f (@allfiles)
{
    if (!(($f eq ".") || ($f eq "..")))
    {
        my $ObjectClass = "Excel.Application";
		my $infile  = $in."\\".$f;
		print "---------------------------------------------------\n";
		print "$infile\n";

		$ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;}) or die "Oops, cannot start excel";
					
		# Make excel visible
		$ex->{'Visible'} = 1;
		sleep(2);

		if (defined $ex)
		{	
			if (my $File = $ex->Workbooks->Open($infile))
			{
				print("File opened successfully\n");
			}
			else
			{
				warn("Unable to open workbook: ", Win32::OLE->LastError());			
			}
		}
		else
		{
			die "Unable to get Excel! $!/n";
		}
		$ex->Quit();
		$ex = undef;
    }
}