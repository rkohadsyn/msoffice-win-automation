Sub openWordFiles(dataFolderIn)
Set fso = CreateObject ("Scripting.FileSystemObject")
Set stdout = fso.GetStandardStream (1)
Set stderr = fso.GetStandardStream (2)

	Set objShell = CreateObject("WScript.Shell")
	Set objExec = objShell.Exec("taskkill /IM WINWORD.EXE")
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objDataFolderIn = objFSO.GetFolder(dataFolderIn)
	Set colFiles = objDataFolderIn.Files
	Dim fileExt
	For Each objFile in colFiles
		fileExt = Split(objFile.Name,".")
		If (fileExt(1) = "doc" Or fileExt(1) = "docx") Then
			stdout.WriteLine objFile.Name
			openWordFile objFile
		End If
	Next
	Set objShell = nothing
	Set objExec = nothing
	Set objFSO = nothing
End Sub


Sub openWordFile(objFile)
		Set objShell = CreateObject("WScript.Shell")
		Set objWord = CreateObject("Word.Application")
		objWord.Visible = TRUE
		Set objDoc = objWord.Documents.Open(objFile.Path)
		WScript.Sleep 2000
		objDoc.Close
		objWord.Quit
		Set objShell = nothing
		Set objWord = nothing
		Set objDoc = nothing
		Set objExec = nothing
End Sub


On Error Resume Next
Dim dataFolderIn
dataFolderIn = wscript.arguments.item(0)
openWordFiles dataFolderIn
