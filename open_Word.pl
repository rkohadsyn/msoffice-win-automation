use Win32::OLE;
use Win32::OLE qw(in with);
use Win32::OLE::Variant;
use Win32::OLE::Const 'Microsoft Word';

chomp ($in = $ARGV[0]);
print "in = $in\n";

opendir (SOURCE,$in) or die $!;
@allfiles = readdir(SOURCE);
closedir(SOURCE);

foreach $f (@allfiles)
{
    if (!(($f eq ".") || ($f eq "..")))
    {
        my $ObjectClass = "Word.Application";
		my $infile  = $in."\\".$f;
		print "---------------------------------------------------\n";
		print "$infile\n";

		$ex = Win32::OLE->new($ObjectClass, sub {$_[0]->Quit;}) or die "Oops, cannot start Word";
					
		# Make Word visible
		$ex->{'Visible'} = 1;
		sleep(2);

		if (defined $ex)
		{	
			if (my $File = $ex->Documents->Open($infile))
			{
				print("File opened successfully\n");
			}
			else
			{
				warn("Unable to open document: ", Win32::OLE->LastError());
			}
		}
		else
		{
			die "Unable to get Word! $!/n";
		}
		$ex->Quit();
		$ex = undef;
    }
}